var rutas = require( "../rutas.json" );
var valores = require( "../valores.json" );

var gulp = require( "gulp" );

var utiles = require("./utiles.js");

gulp.task( "css", function() {
	var stylus = require( "gulp-stylus" );
	var sourcemaps = require( "gulp-sourcemaps" );
	var plumber = require( "gulp-plumber" );
	var nib = require( "nib" );
	var rupture      = require('rupture');

	gulp.src( rutas.origen.stylus + "*.styl" )
		.pipe( plumber({
			errorHandler: utiles.controladorError
		}))
		.pipe( sourcemaps.init() )
    	.pipe( stylus({
    		use: [ rupture(), nib() ],
    		linenos: true
    	}))
    	.pipe( sourcemaps.write() )
    	.pipe( plumber.stop() )
    	.pipe( gulp.dest( rutas.destino.css ));
});
