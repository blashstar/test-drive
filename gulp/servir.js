var rutas = require( "../rutas.json" );
var valores = require( "../valores.json" );

var gulp = require( "gulp" );

gulp.task('servir', ["html", "css", "js"], function(){
	var browserSync = require( "browser-sync" );

    browserSync({
        server: rutas.destino.html,
        directory: true,
        startPath: "/index.html",
        xip: true,
        online: true,
        open: "external",
        logLevel: "info",
        logConnections: true,
        logFileChanges: true,
        logPrefix: "{ " + valores.titulo + " }",
        notify: false,
        scrollProportionally: false,
        scrollRestoreTechnique: "cookie",
        reloadOnRestart: false,
        reloadDelay: 250,
        reloadDebounce: 1000,
        injectChanges: true,
        files: rutas.destino.html + "**/*",
        minify: false,
        codeSync: true
    });

    gulp.watch( rutas.origen.jade + "**/*.jade", ["html"] );
    gulp.watch( rutas.origen.stylus + "**/*.styl", ["css"] );
    gulp.watch( rutas.origen.ecmascript + "**/*.js", ["js"] );
});
