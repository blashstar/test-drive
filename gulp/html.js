var rutas = require( "../rutas.json" );
var valores = require( "../valores.json" );

var gulp = require( "gulp" );

var utiles = require("./utiles.js");

gulp.task( "html", function() {

	var jade = require( "gulp-pug" );
	var sourcemaps = require( "gulp-sourcemaps" );
	var plumber = require( "gulp-plumber" );

	gulp.src( rutas.origen.jade + "*.jade" )
		.pipe( plumber({
			errorHandler: utiles.controladorError
		}))
		.pipe( sourcemaps.init() )
		.pipe( jade({
			basedir: rutas.origen.jade,
			pretty: true,
			locals: valores
		}))
    	.pipe( sourcemaps.write() )
    	.pipe( plumber.stop() )
		.pipe( gulp.dest( rutas.destino.html ));
});
